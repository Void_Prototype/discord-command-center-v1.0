// Setup Express.js
const express = require('express')
const app = express()

// Configure DotENV
const dotenv = require('dotenv')
dotenv.config({ path: './config.env' })

// Middleware to parse body to JSON for all JSON requests
app.use(express.json())

// Setup Discord.js
require('./discordhandling/discordmain')


// Setup Routes and Open Ports
const port = process.env.PORT || 8000

app.listen(port, () => {
  console.log(`Server is running on PORT: ${port}`)
})

app.use('/api/servers', require('./routes/api/servers'))
app.use('/api/messaging', require('./routes/api/messaging'))
app.use('/api/kick', require('./routes/api/kick'))
app.use('/api/ban', require('./routes/api/ban'))